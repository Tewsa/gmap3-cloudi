$(function(){
    
        $("#coordinates").hide();
        
        var $map = $("#googleMap"), 
        menu = new Gmap3Menu($map),
        current,    // current click event (used to save as start/end position)
        m1,         // marker "from"
        m2;         // marker "to"

        // update marker
        function updateMarker(marker, isM1){
            if (isM1){
                m1 = marker;
            } else {
                m2 = marker;
            }		  
            updateDirections();
        }

        // add marker and manage which one it is (A, B)
        function addMarker(isM1){

            var clear = {name:"marker"};
            if (isM1 && m1) {
                clear.tag = "from";
                $map.gmap3({clear:clear});
            } else if (!isM1 && m2){
                clear.tag = "to";
                $map.gmap3({clear:clear});
            }

            // add marker and store it
            $map.gmap3({
                marker:{
                    latLng:current.latLng,
                    options:{
                        draggable:true,
                        icon:new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/icon_green" + 
                                (isM1 ? "A" : "B") + ".png")
                    },
                    tag: (isM1 ? "from" : "to"),
                    events: {
                        dragend: function(marker, event, context){
                            var map = $(this).gmap3("get"),
                            infowindow = $(this).gmap3({get:"infowindow"});
                            if (infowindow){
                                infowindow.close();
                            }
                            updateMarker(marker, isM1);
                        },
                        click: function(marker, event, context){
                            $(this).gmap3({
                                getaddress:{
                                    latLng:marker.getPosition(),
                                    callback:function(results){
                                        var map = $(this).gmap3("get"),
                                        infowindow = $(this).gmap3({get:"infowindow"}),
                                        content = results && results[1] 
                                            ? results && results[1].formatted_address 
                                            : "no address";
                                        if (infowindow){
                                            infowindow.close();
                                            infowindow.open(map, marker);
                                            infowindow.setContent(content);
                                        } else {
                                            $(this).gmap3({
                                                infowindow:{
                                                    anchor:marker, 
                                                    options:{content: content}
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    },
                    callback: function(marker){
                        updateMarker(marker, isM1);
                    }
                }
            });

        } // addMarker
        
        // refresh
        function refreshPage(){
            history.go(0);
        }
        
        // function called to update direction is m1 and m2 are set
        function updateDirections(){
            if (!(m1 && m2)){
                return;
            }
            $map.gmap3({
                getroute:{
                    options:{
                        origin:m1.getPosition(),
                        destination:m2.getPosition(),
                        travelMode: google.maps.DirectionsTravelMode.WALKING
                    },
                    callback: function(results){
                        if (!results) return;
                        $map.gmap3({
                            get:"directionsrenderer"}).setDirections(results);
                    }
                }
            });
        }

        // MENU : ITEM 1
        menu.add("Lähtöpiste", "itemA", 
        function(){
            menu.close();
            addMarker(true);
        });

        // MENU : ITEM 2
        menu.add("Määränpää", "itemB separator", 
        function(){
            menu.close();
            addMarker(false);
        });

        // MENU : ITEM 3
        menu.add("Zoomaa isommaksi", "zoomIn", 
        function(){
            var map = $map.gmap3("get");
            map.setZoom(map.getZoom() + 1);
            menu.close();
        });

        // MENU : ITEM 4
        menu.add("Zoomaa pienemmäksi", "zoomOut separator",
        function(){
            var map = $map.gmap3("get");
            map.setZoom(map.getZoom() - 1);
            menu.close();
        });

        // MENU : ITEM 5
        menu.add("Keskitä", "centerHere separator", 
        function(){
            $map.gmap3("get").setCenter(current.latLng);
            menu.close();
        });
        
        // MENU : ITEM 7
        menu.add("Virkistä sivu", "refresh", 
        function(){
            menu.close();
            refreshPage();
        });
        
        // INITIALIZE GOOGLE MAP
        $map.gmap3({
            getgeoloc:{
                callback : function(latLng){
                    if (latLng){
                        $(this).gmap3({
                            marker:{
                                latLng:[65.00609652077961,25.490993499624892],
                                events: {
                                    click: function(marker, event, context){
                                        $(this).gmap3({
                                            getaddress:{
                                                latLng:marker.getPosition(),
                                                callback:function(results){
                                                    var map = $(this).gmap3("get"),
                                                    infowindow = $(this).gmap3({get:"infowindow"}),
                                                    content = results && results[1] 
                                                        ? results && results[1].formatted_address 
                                                        : "no address";
                                                    if (infowindow){
                                                        infowindow.close();
                                                        infowindow.open(map, marker);
                                                        infowindow.setContent(content);
                                                    } else {
                                                        $(this).gmap3({
                                                            infowindow:{
                                                                anchor:marker, 
                                                                options:{content: content}
                                                            }   
                                                        });
                                                    }
                                                    markerSelected(context.id, content);
                                                }
                                            }
                                        });
                                    }
                                }
                            },
                            map:{
                                options:{
                                    zoom: 15
                                },
                                events:{
                                    rightclick:function(map, event){
                                        current = event;
                                        menu.open(current);
                                    },
                                    click: function(){
                                        menu.close();
                                    },
                                    dragstart: function(){
                                        menu.close();
                                    },
                                    zoom_changed: function(){
                                        menu.close();
                                    }
                                }					
                            },

                            // add direction renderer to configure 
                            // options (else, automatically created 
                            // with default options)
                            directionsrenderer:{
                                divId:"directions",
                                options:{
                                    preserveViewport: true,
                                    markerOptions:{
                                        visible: false,
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        $("#hidecoords").click(function(){
            $("#coordinates").hide();
        });
        
    });

    function markerSelected(id, content){
        $("#coordinates").show();
        var marker = $('#googleMap').gmap3({get:id});
        $("#address .label").text("Osoite: ");
        $("#address .value").text(content);
        $("#latitude .label").text("Latitude: ");
        $("#latitude .value").text(marker.getPosition().lat());
        $("#longitude .label").text("Longitude: ");
        $("#longitude .value").text(marker.getPosition().lng());
    }